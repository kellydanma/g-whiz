package quiz

import (
	"os"
	"testing"
	"time"

	"gotest.tools/assert"
)

const defaultCSV = "./default.csv"

func defaultProblems() []Problem {
	return []Problem{
		{"5+1", "6"},
		{"2+4", "5"},
		{"3+3", "6"},
	}
}

func defaultTimer() *time.Timer {
	return time.NewTimer(time.Duration(1) * time.Second)
}

func createDefaultCSV() {
	f, _ := os.Create(defaultCSV)
	data := "5+1,6\n2+3,5\n3+3,6"
	_, _ = f.WriteString(data)
	_ = f.Close()
}

func cleanup() {
	_ = os.Remove(defaultCSV)
}

func TestGetProblems(t *testing.T) {
	createDefaultCSV()
	tests := []struct {
		FilePath    string
		ErrExpected error
	}{
		{defaultCSV, nil},
		{"filedoesnotexit.csv", errParseCSV},
	}
	for _, test := range tests {
		file, _ := os.Open(test.FilePath)
		_, err := GetProblems(file, true)
		assert.Equal(t, test.ErrExpected, err)
	}
	cleanup()
}

func TestExercise(t *testing.T) {
	sExpected := "\nYou scored 0 out of 3 questions, better luck next time!\n"
	sResult := Exercise(defaultProblems(), defaultTimer())
	assert.Equal(t, sExpected, sResult)
}

func TestPrintScore(t *testing.T) {
	tests := []struct {
		Correct int
		Total   int
		Message string
	}{
		{1, 12, "\nYou scored 1 out of 12 questions, better luck next time!\n"},
		{12, 12, "\nYou scored 12 out of 12 questions, well done!\n"},
	}
	for _, test := range tests {
		m := printScore(test.Correct, test.Total)
		assert.Equal(t, test.Message, m)
	}
}
