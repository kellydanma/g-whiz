package quiz

import (
	"encoding/csv"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

// Problem represents the question and answer as
// strings.
type Problem struct {
	Question string
	Answer   string
}

var (
	errParseCSV = errors.New("failed to parse CSV file")
)

// GetProblems builds a slice of Problems, given
// a slice of string slices.
func GetProblems(file *os.File, randomize bool) ([]Problem, error) {
	r := csv.NewReader(file)
	lines, err := r.ReadAll()
	if err != nil {
		return nil, errParseCSV
	}
	ret := make([]Problem, len(lines))
	for i, line := range lines {
		ret[i] = Problem{
			Question: strings.TrimSpace(line[0]),
			Answer:   strings.TrimSpace(line[1]),
		}
	}
	if randomize {
		ret = shuffle(ret)
	}
	return ret, nil
}

// Exercise prompts the user to solve questions,
// then returns the number of correct responses.
func Exercise(problems []Problem, timer *time.Timer) string {
	var correct int
	for i, p := range problems {
		fmt.Printf("Problem #%d: %s = ", i+1, p.Question)
		answerCh := make(chan string)
		var answer string
		go func() {
			fmt.Scanf("%s\n", &answer)
			answerCh <- answer
		}()
		select {
		case <-timer.C:
			return printScore(correct, len(problems))
		case answer := <-answerCh:
			if answer == p.Answer {
				correct++
			}
		}
	}
	return printScore(correct, len(problems))
}

func printScore(correct, total int) string {
	ret := fmt.Sprintf("\nYou scored %d out of %d questions", correct, total)
	if float64(correct)/float64(total) > 0.7 {
		ret += ", well done!\n"
	} else {
		ret += ", better luck next time!\n"
	}
	return ret
}

func shuffle(p []Problem) []Problem {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	for n := len(p); n > 0; n-- {
		randIndex := r.Intn(n)
		p[n-1], p[randIndex] = p[randIndex], p[n-1]
	}
	return p
}
