package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"gitlab.com/kellydanma/g-whiz/quiz"
)

var (
	flagCSVPath string
	flagRandom  bool
	flagTime    int
)

func init() {
	flag.StringVar(&flagCSVPath, "csv", "problems.csv", "path/to/csv_file")
	flag.BoolVar(&flagRandom, "random", true, "randomize order of questions")
	flag.IntVar(&flagTime, "limit", 20, "test duration")
	flag.Parse()
}

func main() {
	file, err := os.Open(flagCSVPath)
	if err != nil {
		exit(fmt.Sprintf("Failed to open the CSV file: %s\n", flagCSVPath))
	}
	problems, err := quiz.GetProblems(file, flagRandom)
	if err != nil {
		exit(fmt.Sprintf("%s\n", err))
	}
	timer := time.NewTimer(time.Duration(flagTime) * time.Second)
	result := quiz.Exercise(problems, timer)
	fmt.Print(result)
}

func exit(msg string) {
	fmt.Println(msg)
	os.Exit(1)
}
