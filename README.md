# G, whiz!

A simple command-line quiz program.

## Usage

The application uses the `problems.csv` file by default, which provides a set of addition questions.

```
$ go run cmd/main.go
Problem #1: 1+4 = 5
Problem #2: 8+3 = 11
Problem #3: 3+3 =
You scored 2 out of 12 questions, better luck next time!
```

## Customize

| Flag          | Default         | Usage  |       Example      |
|:-------------:| --------------- | ------ | ------------------ |
| `limit`     | 20 | Indicates the time limit in seconds | `go run cmd/main.go -limit=40` extends the time limit to 40 seconds |
| `csv`     | `problems.csv` | Provides the path of a `.csv` file containing problems to solve | `go run cmd/main.go -csv="newfile.csv"` populates the application with questions from a new file |
| `random` | `true` | Randomizes the order of questions to be solved | `go run cmd/main.go -random=false` disables shuffling of questions |
